from django.urls import path
from todos.views import list
from .views import TodoListDetailView
from .views import TodoListCreateView
from .views import TodoListUpdateView

urlpatterns = [
    path("<int:id>/edit/", TodoListUpdateView.as_view(), name="todo_list_update"),
    path("create/", TodoListCreateView.as_view(), name="todo_list_create"),
    path("<int:id>/", TodoListDetailView.as_view(), name="todo_list_detail"),
    path("", list, name="todo_list_list"),
    ]