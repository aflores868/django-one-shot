from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView
from .models import TodoList
from django.views.generic import DetailView
from django.views.generic import CreateView
from .models import TodoList
from django.urls import reverse_lazy
from django.views.generic import UpdateView


#  Create your views here.
class TodoListView(ListView):
     model = TodoList
     template_name = "todos/todo_list.html"

class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/todo_list_detail.html"

class TodoListCreateView(CreateView):
    model = TodoListfields = ['name']
    uscess_url = reverse_lazy('todo_list_list')

class TodoListUpdateView(UpdateView):
    model = TodoList
    field = ['name']
    success_url = reverse_lazy('todo_list_list')

def list(request):
    todos = TodoList.objects.all()
    context = { "todos":
               todos}
    return render(request, 'todos/todos_list.html', context)

